![Makaboom Logo](http://storage.googleapis.com/cr-resource/image/1cd2283e919ed3d99c17fa0e67901dec/makaboom/71d94c3671a07171ce7639ab08e52ac0.png)

# Hockey Bot

This project uses the servo motor to implement the hockey stick motion (have the arching motion between 0 and 214 degrees), in addition to the DC motors used to control the robot back/forth, left/right.

![Robot Pic](img/hockeybot.jpg)

Movements involved

* Move left
* Move right
* Move forward
* Move backward
* Hockey slapping motion

## Rogic Program Snapshot

![Rogic Snapshot 1](img/01.png)
![Rogic Snapshot 2](img/02.png)

